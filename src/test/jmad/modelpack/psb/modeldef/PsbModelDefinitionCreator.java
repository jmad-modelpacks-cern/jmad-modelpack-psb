/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package jmad.modelpack.psb.modeldef;

import static cern.accsoft.steering.jmad.tools.modeldefs.creating.ModelDefinitionCreator.scanDefault;

public class PsbModelDefinitionCreator {
    
    public static void main(String[] args) {
        scanDefault().and().writeTo("src/java/jmad/modelpack/psb/modeldef");
    }

}
