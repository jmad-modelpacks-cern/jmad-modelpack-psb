/**
 * 
 */
package jmad.modelpack.psb.modeldef;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;

import cern.accsoft.steering.jmad.tools.modeldefs.creating.lang.JMadModelDefinitionDslSupport;

/**
 * Defines the model for the LBE transfer line.
 * 
 * @author Kajetan Fuchsberger (kajetan.fuchsberger at cern.ch)
 */
public class PsbModelDefinitionFactory extends JMadModelDefinitionDslSupport {

    {
        name("PSB");

       

        init(i -> {
            i.call("jmad/psb-init.madx");
            i.call("aperture/psb.dbx");
            i.call("elements/psb.ele");
            i.call("sequence/psb.seq");
            i.call("beam/psb_injection.beamx");
        });
        

        sequence("psb1").isDefault().isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.closedOrbit();
                    t.calcAtCenter();
                });
            });
        });
        
        sequence("psb2").isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.closedOrbit();
                    t.calcAtCenter();
                });
            });
        });
        
        sequence("psb3").isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.closedOrbit();
                    t.calcAtCenter();
                });
            });
        });
        
        sequence("psb4").isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.closedOrbit();
                    t.calcAtCenter();
                });
            });
        });

        optics("injection").isDefault().isDefinedAs(o -> {
            o.call("strength/psb_orbit.str").parseAs(STRENGTHS);
            o.call("strength/psb_injection.str").parseAs(STRENGTHS);
            o.call("jmad/match-inj-tunes.madx");
            o.call("beam/ps_injection.beamx");
        });
        
        optics("extraction").isDefinedAs(o -> {
            o.call("strength/psb_orbit.str").parseAs(STRENGTHS);
            o.call("strength/psb_extraction.str").parseAs(STRENGTHS);
            o.call("jmad/match-extr-tunes.madx");
            o.call("beam/ps_extraction.beamx");
        });
        
        optics("acceleration").isDefinedAs(o -> {
            o.call("strength/psb_orbit.str").parseAs(STRENGTHS);
            o.call("jmad/match-acc-tunes.madx");
            o.call("jmad/ps_orbit.madx");
        });

    }

}
